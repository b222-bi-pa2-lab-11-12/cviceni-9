#include <iostream>
#include <cassert>
#include <string>

#include "../warmup/swap.cpp"

template<typename T>
class TTT {
    T x;
public:
    explicit TTT(T x) : x(x) {}
    const T & getX() const { return x; }
};

int main() {
    int x = 10, y = 20;
    Swap(x, y);
    assert(x == 20 && y == 10);
    Swap(x, y);
    assert(x == 10 && y == 20);
    x = y;
    Swap(x, y);
    assert(x == 20 && y == 20);

    std::string s1 = "a", s2 = "b";
    Swap(s1, s2);
    assert(s1 == "b" && s2 == "a");

    TTT<int> x1(10), x2(20);
    Swap(x1, x2);
    assert(x1.getX() == 20 && x2.getX() == 10);

    std::cout << "Uspech" << std::endl;

}