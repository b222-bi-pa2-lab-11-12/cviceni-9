#include <iostream>
#include <cassert>
#include <string>

#include "../cv2/CVector.h"

template<typename T>
class TTT {
    T x;
public:
    explicit TTT(T x, int, int, int) : x(x) {}
    explicit TTT() : x() {}
    const T & getX() const { return x; }
};


int main() {
    CVector<TTT<std::string>> a{};
    a.emplaceBack("aaaa", 1,1,1);
    assert(a.last().getX() == "aaaa");
    a.emplaceBack("b", 1,1,1);
    assert(a.last().getX() == "b");
    a.emplaceBack();
    assert(a.last().getX() == "");

    std::cout << "Uspech" << std::endl;
}