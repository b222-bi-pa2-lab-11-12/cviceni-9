#include <iostream>
#include <cassert>
#include <string>

#include "../cv5/CSharedPtr.h"

template<typename T>
class TTT {
    T x;
public:
    TTT() {}
    explicit TTT(T x) : x(x) {}
    const T & getX() const { return x; }
};


int main() {
    CSharedPtr<TTT<std::string>> a{};
    CSharedPtr<TTT<int>> b{};
    CSharedPtr<TTT<double>> c{};
    CSharedPtr<std::string> d{};
    CSharedPtr<int> e{};
    CSharedPtr<double> f{};

    std::cout << "Uspech" << std::endl;
}
