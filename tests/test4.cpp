#include <iostream>
#include <cassert>
#include <string>

#include "../cv5/CSharedPtr.h"

template<typename T>
class TTT {
    T x;
public:
    explicit TTT(T x, int, int, int) : x(x) {}
    explicit TTT() : x() {}
    const T & getX() const { return x; }
};


int main() {
    auto x = makeShared<TTT<std::string>>("aaaa", 1,1,1);
    auto y = makeShared<TTT<std::string>>();
    assert(x->getX() == "aaaa" && y->getX() == "");

    std::cout << "Uspech" << std::endl;
}
