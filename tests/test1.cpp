#include <iostream>
#include <cassert>
#include <string>

#include "../cv2/CVector.h"

template<typename T>
class TTT {
    T x;
public:
    TTT() {}
    explicit TTT(T x) : x(x) {}
    const T & getX() const { return x; }
};


int main() {
    CVector<TTT<std::string>> a{};
    CVector<TTT<int>> b{};
    CVector<TTT<double>> c{};
    CVector<std::string> d{};
    CVector<int> e{};
    CVector<double> f{};

    std::cout << "Uspech" << std::endl;
}
