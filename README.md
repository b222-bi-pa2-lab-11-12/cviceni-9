# Cvičení 9

Dnes to bude takové opáčko... Vrátíme se k historickým úlohám a zkusíme je vylepšit o úžasné vlastnosti šablon.

## Něco na rozehřátí

Po ránu je prý dobré se zahýbat. Toto ale není hodina tělocviku, budeme tedy hýbat hodnotami proměnných, *prohazovat* je.

Toto prohození budeme chtít mít k dispozici pro mnoho datových typů. Takže abychom se neupsali a nevyužívali ošklivých přetypování, napíšeme generickou funkci pomocí template.

Napsanou funkcí se následně pokocháme, a zamyslíme se, zda implementace nevyžaduje příliš kopírování v místech, kde by postačoval přesun. Pokud si takové chyby všimneme, pokýveme hlavou a implementaci upravíme.



## Úkoly na toto cvičení

### 1) šablonizace vectoru

Ve složce [cv2](cv2) naleznete kód Vektoru z druhého cvičení. Nyní již máte znalosti na to, abyste upravili implementaci generickou.

Upravte vektor tak, aby se jednalo o šablonu, která bude schopná ukládat sekvenci prvků podle předem neznámého typu (uvedeným v `<>`závorkách za `CVector`)

### 2) Přidejte metodu emplaceBack do vektoru

Tato metoda bude přijímat předem neznámí seznam / počet argumentů. Místo toho, aby byl metode předán prvek pro vložení (jako to dělá pushBack),
tak emplace back za pomocí argumentů provolá konstruktor typu vectoru a jako argumenty použije právě argumenty svoje. Nově vytvořený prvek vloží do vectoru.

### 3) Šablonizujte CSharedPtr z 5. cviceni 
Ve složce [cv5](cv5) naleznete kód shared ptr z pátého cvičení. Předdělejte ho zase na všeobecnou šablonu, jako jsme to udělali u vectoru.

### 4) Vytvořte funkci make shared
Vytvořte funkci make shared, která sama naalokuje prvek (stejne jako u emplace back to provede z parametrů co příjme (neznámý počet)) a ten vloží následně do
shared ptr, který vrátí.

### 5) Vytvořte ve vectoru metodu map
Tato metoda příjme parametrem funkci / instanci funktoru / lambdu etc... která jako parametr příjme jeden prvek z vectoru a vrátí jiný (a klidně i jiného datového typu).

Funkce map vrátí vector právě těchto jiných prvků, v pořadí, jak byli ve vectoru původním.

## Pro rychlejší
Máme v hlavě několik výzev, které se dají na cvičení uskutečnít, nebojte se o nějakou z nich poprosit ;)






