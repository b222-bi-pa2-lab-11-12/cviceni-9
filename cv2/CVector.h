#ifndef INTVEC_H
#define INTVEC_H

#include <cstdlib>
#include <iostream>

typedef int TYPE;

const size_t DEFAULT_SIZE = 10;

class CVector {
private:
    TYPE * m_buf;
    size_t m_len;
    size_t m_cap;

    static size_t m_count_at;
public:
    explicit CVector(size_t cap = 10);
    ~CVector();
    [[nodiscard]] const TYPE & at(size_t i) const;
    [[nodiscard]] const TYPE & last() const;
    [[nodiscard]] TYPE & at(size_t i);
    [[nodiscard]] TYPE & last();
    CVector & pushBack(const TYPE & i);
    static size_t getCountAt();

    //TODO emplaceBack() method
};

#endif //INTVEC_H
