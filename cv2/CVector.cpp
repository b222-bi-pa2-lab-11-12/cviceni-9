
#include "CVector.h"

CVector::CVector(size_t cap) {
    m_buf = new TYPE[cap];
    m_len = 0;
    m_cap = cap;
}

CVector::~CVector() {
    delete[] m_buf;
}

const TYPE & CVector::at(size_t i) const {
    m_count_at++;
    return m_buf[i];
}

const TYPE & CVector::last() const {
    return at(m_len - 1);
}

TYPE & CVector::at(size_t i) {
    m_count_at++;
    return m_buf[i];
}

TYPE & CVector::last() {
    return at(m_len - 1);
}

CVector &CVector::pushBack(const TYPE & i) {
    if (m_len == m_cap) {
        size_t new_cap = m_cap * 2;
        TYPE *new_buf = new TYPE [new_cap];
        std::copy(m_buf, m_buf + m_len, new_buf);
        delete[] m_buf;
        m_buf = new_buf;
        m_cap = new_cap;
    }
    m_buf[m_len] = i;
    m_len++;

    return *this;
}

size_t CVector::m_count_at = 0;

size_t CVector::getCountAt() {
    return m_count_at;
}
