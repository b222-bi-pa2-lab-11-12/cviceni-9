#include <iostream>

#ifndef CVICENI_9_CSHAREDPTR_H
#define CVICENI_9_CSHAREDPTR_H


class CSharedPtr {
    struct CSharedHolder {
        size_t m_count = 1;
        CVector *m_ptr;
        explicit CSharedHolder(CVector *mPtr) : m_ptr(mPtr) {}
    };
    CSharedHolder *m_holder = nullptr;
    void copyFrom(const CSharedPtr &original) {
        m_holder = original.m_holder;
        if (m_holder) {
            m_holder->m_count++;
        }
    }
    void clear() {
        if (m_holder && !--m_holder->m_count) {
            delete m_holder->m_ptr;
            delete m_holder;
        }
        m_holder = nullptr;
    }
public:
    explicit CSharedPtr(CVector *ptr = nullptr) {
        if (ptr)
            m_holder = new CSharedHolder(ptr);
    }
    CSharedPtr(const CSharedPtr &original) { copyFrom(original); }
    CSharedPtr(CSharedPtr &&original) noexcept :m_holder(original.m_holder) { original.m_holder = nullptr; }
    virtual ~CSharedPtr() { clear(); }
    CSharedPtr &operator=(const CSharedPtr &original) {
        if(this == &original)
            return *this;
        clear();
        copyFrom(original);
        return *this;
    }
    CVector *operator->() { return m_holder->m_ptr; }
    const CVector *operator->() const { return m_holder->m_ptr; }
    CVector & operator*() { return *m_holder->m_ptr; }
    const CVector & operator*() const { return *m_holder->m_ptr; }
    explicit operator bool() { return m_holder; }
    bool operator !() { return !(bool)m_holder; }
    bool operator==(const CSharedPtr &rhs) const { return m_holder == rhs.m_holder; }
    bool operator!=(const CSharedPtr &rhs) const { return m_holder != rhs.m_holder; }
    [[nodiscard]] size_t count() const {
        if(m_holder)
            return m_holder->m_count;
        return 0;
    }
};

//TODO makeShared() function



#endif //CVICENI_9_CSHAREDPTR_H
